﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="viewDocuments.aspx.cs" Inherits="fse_rec.viewDocuments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="gvUploads" runat="server" AutoGenerateColumns="false" EmptyDataText="No files uploaded">
    <Columns>
        <asp:BoundField DataField="Text" HeaderText="File Name" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnDownload" Text="Download" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="btnDownload_OnClick"></asp:Button>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnDelete" Text="Delete" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="btnDelete_OnClick" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
    <asp:GridView ID="GridView1" runat="server" DataSourceID="gridViewDataSource">
    </asp:GridView>
</asp:Content>
