﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace fse_rec
{
    public partial class upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string extension = System.IO.Path.GetExtension(fileUpload.FileName).ToLower();
                if (fileUpload.HasFile)
                {
                    
                    if (extension == ".zip" || extension == ".pdf" || extension == ".doc" || extension == ".docx")
                    {
                        string fileName = Path.GetFileName(fileUpload.PostedFile.FileName);
                        fileUpload.PostedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        

                        string str = fileUpload.FileName;
                        string doc = "~/Uploads/" + str.ToString();
                        string name = txtName.Text;
                        string stunum = txtStuNum.Text;
                        string title = txtTitle.Text;

                        string connectionString = ConfigurationManager.ConnectionStrings["fse_recConnectionString"].ToString();
                        SqlConnection connection = new SqlConnection(connectionString);
                        {
                            using (SqlCommand command = new SqlCommand())
                            {
                                command.Connection = connection;
                                command.CommandType = CommandType.Text;
                                command.CommandText = "INSERT INTO Uploads (Name, StudentNumber, Title, Doc) VALUES (@Name, @StudentNumber, @Title, @Doc)";
                                command.Parameters.AddWithValue("@Name", name);
                                command.Parameters.AddWithValue("@StudentNumber", stunum);
                                command.Parameters.AddWithValue("@Title", title);
                                command.Parameters.AddWithValue("@Doc", doc);

                                try
                                {
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                }
                                catch(SqlException)
                                {

                                }
                                finally
                                {
                                    connection.Close();
                                    Response.Redirect(Request.Url.AbsoluteUri);
                                }
                            }
                        }
                    }
                    else
                    {
                        //literal code here (wrong filetype)
                    }
                }
                else
                {
                    //literal code here (no file)
                }
                
            }
        }
    }
}