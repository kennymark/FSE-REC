﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fse_rec
{
    public partial class contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnContactSend_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = "ieuanCO5027@gmail.com";
                credentials.Password = "assignment";
                smtpClient.Credentials = credentials;

                MailMessage studentContactEmail = new MailMessage("ieuanCO5027@gmail.com", "1510717@chester.ac.uk");
                studentContactEmail.IsBodyHtml = false;
                studentContactEmail.Subject = "FSE-REC Contact Page Email Message";
                studentContactEmail.Body = "Email received from FSE-REC contact page\n\n" + "\"" + messageBox.Text + "\"" + "\n\nEmail received from student " + nameBox.Text + " (" + studentNoBox.Text + ") from the email address " + emailBox.Text;

                MailMessage nonStudentContactEmail = new MailMessage("ieuanCO5027@gmail.com", "1510717@chester.ac.uk");
                nonStudentContactEmail.IsBodyHtml = false;
                nonStudentContactEmail.Subject = "FSE-REC Contact Page Email Message";
                nonStudentContactEmail.Body = "Email received from FSE-REC contact page\n\n" + "\"" + messageBox.Text + "\"" + "\n\nEmail received from " + nameBox.Text + " from the email address " + emailBox.Text;

                MailMessage confirmationEmail = new MailMessage("ieuanCO5027@gmail.com", emailBox.Text);
                confirmationEmail.IsBodyHtml = false;
                confirmationEmail.Subject = "Your email to FSE-REC has been sent";
                confirmationEmail.Body = "Your email to the FSE Research Ethics Committee has been sent successfully, and a representative will respond to you shortly";

                try
                {
                    if (string.IsNullOrEmpty(studentNoBox.Text))
                    {
                        smtpClient.Send(nonStudentContactEmail);
                    }
                    else
                    {
                        smtpClient.Send(studentContactEmail);
                    }
                    smtpClient.Send(confirmationEmail);
                    litContact.Text = "<p>Your message has been sent successfully, and a confirmation email has been sent to you as well.</p>";
                    messageBox.Text = "";
                }
                catch (Exception ex)
                {
                    litContact.Text = "<p>Sorry, but your message has not been successfully sent. Please try again in a few moments. If the issue persists, please contact an administrator.</p>";
                }
            }
        }
    }
}