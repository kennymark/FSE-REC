﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="fse_rec.contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="ui large aligned center aligned grid">

        <div class="column">

            <h1 class="ui top attached teal header">Contact Page</h1>

            <div class="ui teal compact segment">

                <!--student number-->
                <div class="field">
                    <h3 class="ui teal header">
                        <asp:Label ID="studentNoBoxLbl" AssociatedControlID="studentNoBox" runat="server" Text="Student number (Optional)" CssClass="min_fix_layout"></asp:Label>
                    </h3>
                </div>
                <div class="field">
                    <asp:TextBox ID="studentNoBox" runat="server" CssClass="textb"></asp:TextBox>
                </div>
                <br />

                <!--name-->
                <div class="field">
                    <h3 class="ui teal header">
                        <asp:Label ID="nameBoxLbl" AssociatedControlID="nameBox" runat="server" Text="*Name:"></asp:Label>
                    </h3>
                </div>

                <div class="field">
                    <asp:TextBox ID="nameBox" runat="server" CssClass="textb"></asp:TextBox>
                </div>

                <div class="field">
                    <asp:RequiredFieldValidator ID="nameReqFieldValidator" ControlToValidate="nameBox" runat="server" ErrorMessage="*Do not leave name field blank, please enter your name*" CssClass="text_colour"></asp:RequiredFieldValidator>
                </div>
                <br />

                <!--email-->
                <div class="field">
                    <h3 class="ui teal header">
                        <asp:Label ID="emailBoxLbl" AssociatedControlID="emailBox" runat="server" Text="*Email Address:"></asp:Label>
                    </h3>
                </div>
                <div class="field">
                    <asp:TextBox ID="emailBox" runat="server" CssClass="textb"></asp:TextBox>
                </div>
                <div class="field">
                    <asp:RequiredFieldValidator ID="emailReqFieldValidator" runat="server" ControlToValidate="emailBox" ErrorMessage="*Do not leave email field blank, please enter a email address" CssClass="text_colour"></asp:RequiredFieldValidator>

                    <asp:RegularExpressionValidator ID="emailRegExValidator" runat="server" ControlToValidate="emailBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*" ErrorMessage="Please enter a valid email address"></asp:RegularExpressionValidator>
                </div>

                <!--message-->
                <div class="field">
                    <h3 class="ui teal header">
                        <asp:Label ID="messageBoxLbl" AssociatedControlID="messageBox" runat="server" Text="*Message:"></asp:Label>
                    </h3>
                </div>
                <div class="field">
                    <asp:TextBox ID="messageBox" TextMode="MultiLine" runat="server" CssClass="textb"></asp:TextBox>
                </div>
                <div class="field">
                    <asp:RequiredFieldValidator ID="messageReqFieldValidator" runat="server" ControlToValidate="messageBox" ErrorMessage="*Do not leave the message field blank, please enter a message you would like to send" CssClass="text_colour"></asp:RequiredFieldValidator>
                </div>

                <!--button-->
                <div class="field">
                    <asp:Button ID="btnContactSend" runat="server" Text="Send" OnClick="btnContactSend_Click" class="ui fluid teal submit button" />
                    <asp:Literal ID="litContact" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
